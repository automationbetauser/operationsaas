# **OPERATION SAAS**

## **Overview**

### Pre-requisites for deploying blueprints

* Cloud Assembly Ogananisation configured with cloud accounts configured for one or more cloud endpoints vSphere, AWS, Azure, GCP, VMC.
* All Blueprints use Ubuntu 1604 LTS and cloud-init for bootstrapping. For template preparation, see https://blogs.vmware.com/management/2019/02/building-a-cas-ready-ubuntu-template-for-vsphere.html
* All blueprint tags should be updated to handle target environment placement
* All blueprints should be updated with the relevant SSH keys

## Deployment

Deployment Order

1. Docker Host Blueprint
2. Wavefront Proxy Blueprint
3. Test Runner Blueprint
4. Configure Codestream
5. Import VMware Codestream Custom Integrations
6. Import VMware Codestream Pipeline (update target project in yaml)
7. Update pipeline variables and workspace

### **Docker Host Blueprint**

This blueprint provides a Docker Host, for CICD pipeline runtime execution, this Docker Host is used for CI and Custom Integrations for the Tito pipeline.

artifact: [DocketHost.yml](./Blueprints/DockerHost/DockerHost.yml)

### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Pre-requisites

* outbound connectivity to the internet - poll public IP for Tito application

### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Steps to deploy

* Import Blueprint to deployment project
* Update blueprint with tags for placement on vSphere environment
* Deploy blueprint
* Validate VMware Code Stream connectivity via proxy agent by creating endpoint

### Wavefront Proxy Blueprint
This blueprint deploys and configures a wavefront proxy for metric forwarding to the wavefront portal. 

*Note: For this deployment a Wavefront proxy will be deployed to AWS with a public IP.*

artifact: [WavefrontProxy.yml](./Blueprints/WavefrontProxy/WavefrontProxy.yml)

### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Pre-requisites

* Wavefront Account access
* Wavefront portal URL: vmware.wavefront.com
* Wavefront API Token:

**Generating an API Token**

A Wavefront API token is a string of hexadecimal characters and dashes. For example:

> a411c16b-3cf7-4f03-bf11-8ca05aab898d

**To generate an API token:**

In the Wavefront UI, click the gear icon  at the top right of the task bar and select your username.
At the bottom of your user profile, locate the section API Access.
Click Generate. You can have up to 2 tokens at any given time. If you want to generate a new token but already have two tokens, then you must revoke one of the existing tokens.
To revoke a token, click the Revoke link next to the token. If you run a script that uses a revoked token, the script returns an authorization error.



**Creating a Docker Host Endpoint in VMware Code Stream**

* Login to VMware Code Stream via Cloud Services Portal https://console.cloud.vmware.com
* Click Enpoints > New Endpoint

* Specify target project and relevant cloud proxy for onpremise connectivity
* Default docker remote API is configured to listen on 2375

**Example Docker endpoint configuration**

![DockerHostEndpoint](images/DockerHostEndpoint.png)




### **Test Runner Blueprint**

This blueprint deploys and configures [Cypress](https://www.cypress.io/) for UI test automation and [Locust](https://locust.io/) for load testing. Test execution is via VMware Code Stream pipeline using an SSH agent.
)
artifact: [Cypress_Locust.yml](.\Blueprints\TestRunner\Cypress_Locust.yml)

### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Steps to deploy

* Import Blueprint to deployment project 
* Update blueprint with tags for placement on vSphere environment
* Deploy blueprint
* Create SSH Endpoint in Codestream 
* Validate VMware Code Stream connectivity via proxy agent. Create basic pipeline step, configure SSH task with echo "Test"

**Creating SSH Endpoint in VMware Code Stream**

Log into VMware Code Stream, Endpoints > New Endpoint

![SSHHostEndpoint](images/SSHHostEndpoint.png)


## **VMware Code Stream Pipeline**

artifact:
[Validate_Application_Tito](Codestream/Pipelines/Validate_Application%20_Tito.yaml)

### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Pre-requisites

* Test Runner Blueprint Deployed (Cypress + Locust). See /Assets/Blueprints/TestRunner/Cypress_Locust.yml
* Docker Host Blueprint Deployed see /Assets/Blueprints/DockerHost/DockerHost.yml
* SSH key or Account pw details for this testrunner host
* Connectivity from test runner to web server test will be executed against
* Slack bot OAuth token for notifications

Overview

![pipeline overview](images/pipeline.png)

## **Action Based Extensibilty - Service Now Integration**

### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pre-requisites

* ServiceNow Instance
* ServiceNow Admin account for API access
* InstanceURL
* Cloud Services Portal (CSP) API Token  for Cloud Assembly API