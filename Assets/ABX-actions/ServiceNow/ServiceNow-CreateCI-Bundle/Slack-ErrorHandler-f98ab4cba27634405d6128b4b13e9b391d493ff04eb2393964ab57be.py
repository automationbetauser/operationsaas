from botocore.vendored import requests
import json

def handler(context, inputs):

    errorMsg = inputs["errorMsg"]
    flowInputs = inputs["flowInputs"]
    print("Flow execution failed with error {0}".format(errorMsg))
    print("Flow inputs were: {0}".format(flowInputs))
    
    
    url = "https://slack.com/api/chat.postMessage"
    headers = {'Content-Type': 'application/json; charset=utf-8', 
               'Accept': 'application/json',
               'Authorization': 'Bearer ' + inputs['auth_token'] # Auth Token for Slack App Bot or User
               }
    payload = {
            'channel': inputs['channel'], # Slack user or channel, where you want to send the message
            #'thread_ts': inputs['slackThread'],
            'attachments': [
                {
                    'fallback': inputs['message'] + "action flow execution failed with error {0}".format(errorMsg),
                    'color': inputs['colorHex'],
                    'title': inputs['messageHeading'],
                    'text': inputs['message'] + " action flow execution failed with error {0}".format(errorMsg)
                    #'ts': inputs['slackThread'] 
                }
            ]
        }
    
    results = requests.post(
        url,
        json=payload,
        headers=headers,
    )
    print(results.text)
    
    outputs = {
        "errorMsg": errorMsg,
        "flowInputs": flowInputs
    }
    return outputs