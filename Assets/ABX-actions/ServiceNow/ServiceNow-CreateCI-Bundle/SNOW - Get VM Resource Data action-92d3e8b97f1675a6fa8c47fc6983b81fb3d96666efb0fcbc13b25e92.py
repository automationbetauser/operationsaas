from botocore.vendored import requests
import json
#import boto3 ## For SSM
#client = boto3.client('ssm','ap-southeast-2')

def handler(context, inputs):
    baseUri = inputs['url']
    #casToken = client.get_parameter(Name="casToken",WithDecryption=True) For AWS SSM
    casToken = inputs['casToken']
    url = baseUri + "/iaas/login"
    headers = {"Accept":"application/json","Content-Type":"application/json"}
    #payload = {"refreshToken":casToken['Parameter']['Value']} ## if using AWS SSM
    payload = {"refreshToken":casToken}
    
    results = requests.post(url,json=payload,headers=headers)
    
    bearer = "Bearer "
    bearer = bearer + results.json()["token"]
    
    deploymentId = inputs['deploymentId']
    resourceId = inputs['resourceIds'][0]
    
    print("deploymentId: "+ deploymentId)
    print("resourceId:" + resourceId)
    
    machineUri = baseUri + "/iaas/machines/" + resourceId
    headers = {"Accept":"application/json","Content-Type":"application/json", "Authorization":bearer }
    resultMachine = requests.get(machineUri,headers=headers)
    print("machine: " + resultMachine.text)
    
    print( "serviceNowCPUCount: "+ json.loads(resultMachine.text)["customProperties"]["cpuCount"] )
    print( "serviceNowMemoryInMB: "+ json.loads(resultMachine.text)["customProperties"]["memoryInMB"] )
    
    #update customProperties for use in action flow
    outputs = {
        "cpuCount": int(json.loads(resultMachine.text)["customProperties"]["cpuCount"]),
        "memoryInMB": json.loads(resultMachine.text)["customProperties"]["memoryInMB"]
    }
    return outputs